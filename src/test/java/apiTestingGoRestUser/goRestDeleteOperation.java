package apiTestingGoRestUser;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

public class goRestDeleteOperation {
  @Test
  public void deleteUser() {
	  baseURI="https://gorest.co.in";
	  given().header("Authorization","Bearer af52f55e082382eb807b4ac58cf064339cf822d07eea1f1974f83da11e407c82")
	  .when().delete("/public/v2/users/179076").then().statusCode(204);
  }
}
