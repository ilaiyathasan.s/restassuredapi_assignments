package apiTestingGoRestUser;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class goRestPutOperation {
  @Test
  public void updateUserData() {
	  JSONObject request = new JSONObject();
	  request.put("name", "ravikumar");
	  request.put("email", "ravikumar24@gmail.com");
	  request.put("gender", "male");
	  request.put("status", "active");
	  
	  
	  baseURI="https://gorest.co.in";
	  given().header("Authorization","Bearer af52f55e082382eb807b4ac58cf064339cf822d07eea1f1974f83da11e407c82")
	  .contentType("application/json")
	  .body(request.toJSONString()).when().put("/public/v2/users/179076")
	  .then().statusCode(200).log().all();
  }
}
