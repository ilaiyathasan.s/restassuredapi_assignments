package apiTestingGoRestUser;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class goRestStatusCodeTesting {
	
  @Test
  public void getStatusCode() {
	  baseURI="https://gorest.co.in";
	  given().when().get("/public/v2/users").then().statusCode(200).log().all();
  }
  
}
