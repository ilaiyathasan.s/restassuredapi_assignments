package apiTestingGoRestUser;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class goRestPostOperation {
  @Test
  public void createUser() {
	  JSONObject request = new JSONObject();
	  request.put("name", "ravi");
	  request.put("email", "ravi24@gmail.com");
	  request.put("gender", "male");
	  request.put("status", "active");
	  
	  
	  baseURI="https://gorest.co.in";
	  given().header("Authorization","Bearer af52f55e082382eb807b4ac58cf064339cf822d07eea1f1974f83da11e407c82")
	  .contentType("application/json")
	  .body(request.toJSONString()).when().post("/public/v2/users")
	  .then().statusCode(201).log().all();
  }
}
