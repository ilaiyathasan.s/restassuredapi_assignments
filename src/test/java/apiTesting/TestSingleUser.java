package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

public class TestSingleUser {
  @Test
  public void getSingleUser() {
	  baseURI="https://reqres.in";
	  given().get("/api/users/2").then().log().all();
  }
  
  @Test
  public void testLastName() {
	  baseURI="https://reqres.in";
	  given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));	
  }
}
