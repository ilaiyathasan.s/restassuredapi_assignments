package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class TestingPostOperation {
  @Test
  public void postOperationTest() {
	  JSONObject request = new JSONObject();
	  request.put("name", "thasan");
	  request.put("job", "trainer");
	  
	  System.out.println(request);
	  
	  baseURI="https://reqres.in/api";
	  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
  }
}
