package apiTesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class TestingPutRequest {
  @Test
  public void updatingDataUsingPut() {
	  JSONObject request = new JSONObject();
	  request.put("name", "thasan");
	  request.put("job", "trainer");
	  
	  given().body(request.toJSONString()).when().put("https://reqres.in/api/users/2").then().statusCode(200).
	  body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:57:32.995Z"));
  }
}
