package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

public class TestingDeleteRequest {
  @Test
  public void deleteOperation() {
	  baseURI="https://reqres.in/";
	  given().when().delete("api/users/2").then().statusCode(204);
  }
}
