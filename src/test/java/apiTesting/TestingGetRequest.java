package apiTesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestingGetRequest {
  @Test
  public void firstGetMethod() {
	  Response getResponse=RestAssured.get("https://reqres.in/api/users?page=2"); 
	  int statusCode=getResponse.statusCode();
	  System.out.println(statusCode);
	  System.out.println(getResponse.getBody().asString());
  }
  
  @Test
  public void assertGetMethod() {
	  Response getResponse=RestAssured.get("https://reqres.in/api/users?page=2"); 
	  int statusCode=getResponse.statusCode();
	  Assert.assertEquals(200, statusCode);
  }
}
