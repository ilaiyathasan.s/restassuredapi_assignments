package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class TestValues {
  @Test
  public void testingAValue() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name", hasItem("Rachel"));
  }
  
	  @Test
	  public void testingMultipleValues() {
		  baseURI="https://reqres.in";
		  given().get("/api/users?page=2").then().body("data.first_name", hasItems("Rachel","Lindsay"));
	  }
}
